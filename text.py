import bpy
import math
                
bpy.ops.object.select_by_type(type='MESH')
bpy.ops.object.delete()

bpy.ops.curve.primitive_bezier_circle_add(radius=1)
c = bpy.data.objects['BezierCircle']

bpy.ops.object.text_add(layers=(True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))

bpy.ops.object.mode_set(mode='EDIT')

bpy.ops.font.delete()
bpy.ops.font.text_insert(text="Bent Text", accent=False)

bpy.ops.object.mode_set(mode='OBJECT')

bpy.context.object.data.extrude = 0.03
bpy.ops.object.convert(target='MESH', keep_original=False)
bpy.ops.transform.rotate(value=(3*math.pi/2.0), axis=(1, 0, 0))

bpy.ops.object.mode_set(mode='EDIT')

bpy.ops.mesh.subdivide(number_cuts=3)

bpy.ops.object.mode_set(mode='OBJECT')

bpy.ops.transform.rotate(value=(math.pi), axis=(0, 1, 0))

bpy.context.object.modifiers.new('mycurve', type='CURVE').object = c

bpy.ops.export_mesh.stl(filepath="out.stl")
